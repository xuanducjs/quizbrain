package com.example.brainquiz;

public class CauHoi {
    private String cauHoi, traLoi1, traLoi2, traLoi3, traLoi4;
    private int dapAn;

    public CauHoi() {
    }

    public CauHoi(String cauHoi, String traLoi1, String traLoi2, String traLoi3, String traLoi4, int dapAn) {
        this.cauHoi = cauHoi;
        this.traLoi1 = traLoi1;
        this.traLoi2 = traLoi2;
        this.traLoi3 = traLoi3;
        this.traLoi4 = traLoi4;
        this.dapAn = dapAn;
    }

    public String getCauHoi() {
        return cauHoi;
    }

    public void setCauHoi(String cauHoi) {
        this.cauHoi = cauHoi;
    }

    public String getTraLoi1() {
        return traLoi1;
    }

    public void setTraLoi1(String traLoi1) {
        this.traLoi1 = traLoi1;
    }

    public String getTraLoi2() {
        return traLoi2;
    }

    public void setTraLoi2(String traLoi2) {
        this.traLoi2 = traLoi2;
    }

    public String getTraLoi3() {
        return traLoi3;
    }

    public void setTraLoi3(String traLoi3) {
        this.traLoi3 = traLoi3;
    }

    public String getTraLoi4() {
        return traLoi4;
    }

    public void setTraLoi4(String traLoi4) {
        this.traLoi4 = traLoi4;
    }

    public int getDapAn() {
        return dapAn;
    }

    public void setDapAn(int dapAn) {
        this.dapAn = dapAn;
    }
}
