package com.example.brainquiz;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class Music extends Service {
    MediaPlayer mediaPlayer;
    Boolean isStop = false;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("music ne", "onStartCommand: music");
        String strKey = intent.getExtras().getString("extra");
        if (strKey.equals("on")) {
            isStop = false;
        } else if (strKey.equals("off")) {
            isStop = true;
        }

        if (isStop == false) {
            mediaPlayer = MediaPlayer.create(this, R.raw.cominghome);
            mediaPlayer.start();
            mediaPlayer.setLooping(true);
            isStop = true;
        } else if (isStop == true) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        return START_NOT_STICKY;
    }
}
