package com.example.brainquiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class CauHoiDB {
    SQLiteDatabase database;
    DBHelper dbHelper;

    public CauHoiDB(Context context) {
        dbHelper = new DBHelper(context);
        try {
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLiteException ex) {
            database = dbHelper.getReadableDatabase();
        }
    }

    public void nhapCauHoi() {
        CauHoi cauHoi1 = new CauHoi("1 + 1 bằng bao nhiêu ta?", "4", "2", "7", "5", 2);
        CauHoi cauHoi2 = new CauHoi("1 + 2 bằng bao nhiêu ta?", "4", "2", "3", "5", 3);
        CauHoi cauHoi3 = new CauHoi("1 + 3 bằng bao nhiêu ta?", "4", "0", "7", "6", 1);
        CauHoi cauHoi4 = new CauHoi("1 + 4 bằng bao nhiêu ta?", "4", "2", "8", "5", 4);
        CauHoi cauHoi5 = new CauHoi("1 + 5 bằng bao nhiêu ta?", "6", "2", "7", "5", 1);
        CauHoi cauHoi6 = new CauHoi("1 + 6 bằng bao nhiêu ta?", "7", "2", "9", "5", 1);
        CauHoi cauHoi7 = new CauHoi("1 + 7 bằng bao nhiêu ta?", "4", "8", "7", "0", 2);
        CauHoi cauHoi8 = new CauHoi("1 + 8 bằng bao nhiêu ta?", "3", "1", "7", "9", 4);
        CauHoi cauHoi9 = new CauHoi("1 + 9 bằng bao nhiêu ta?", "6", "2", "5", "10", 4);
        CauHoi cauHoi10 = new CauHoi("1 + 10 bằng bao nhiêu ta?", "4", "1", "11", "5", 3);
        themCauHoi(cauHoi1);
        themCauHoi(cauHoi2);
        themCauHoi(cauHoi3);
        themCauHoi(cauHoi4);
        themCauHoi(cauHoi5);
        themCauHoi(cauHoi6);
        themCauHoi(cauHoi7);
        themCauHoi(cauHoi8);
        themCauHoi(cauHoi9);
        themCauHoi(cauHoi10);
    }

    public long themCauHoi(CauHoi cauHoi) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COT_CAUHOI, cauHoi.getCauHoi());
        values.put(DBHelper.COT_TRALOI1, cauHoi.getTraLoi1());
        values.put(DBHelper.COT_TRALOI2, cauHoi.getTraLoi2());
        values.put(DBHelper.COT_TRALOI3, cauHoi.getTraLoi3());
        values.put(DBHelper.COT_TRALOI4, cauHoi.getTraLoi4());
        values.put(DBHelper.COT_DAPAN, cauHoi.getDapAn());
        return database.insert(DBHelper.TEN_BANG_CAUHOI, null, values);
    }

    public Cursor layCauHoi() {
        // Biến cot là khai báo danh sách các cột cần lấy.
        String[] cot = {
                DBHelper.COT_MACAUHOI,
                DBHelper.COT_CAUHOI,
                DBHelper.COT_TRALOI1,
                DBHelper.COT_TRALOI2,
                DBHelper.COT_TRALOI3,
                DBHelper.COT_TRALOI4,
                DBHelper.COT_DAPAN
        };
        Cursor cursor = null;
        cursor = database.query(DBHelper.
                        TEN_BANG_CAUHOI, cot, null, null, null, null, "RANDOM()");
        return cursor;
    }
}
