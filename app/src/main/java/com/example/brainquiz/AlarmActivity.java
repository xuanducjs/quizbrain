package com.example.brainquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity {

    Animation topAnim, bottomAnim, outTopAnim, outBottomAnim, leftAnim, rightAnim, outLeftAnim, outRightAnim;
    Button btnHenGio, btnDung, btnXemKQ;
    TimePicker timePicker;
    TextView tv1, tv2, tvSlogan1, tvSlogan2;

    Calendar calendar;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    private static int SPLASH_SCREEN = 800;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        setControl();
        setEvent();
    }

    private void setEvent() {
        tvSlogan1.setAnimation(topAnim);
        tvSlogan2.setAnimation(topAnim);
        timePicker.setAnimation(leftAnim);
        btnDung.setAnimation(bottomAnim);
        btnHenGio.setAnimation(bottomAnim);
        btnXemKQ.setAnimation(bottomAnim);
        tv1.setAnimation(leftAnim);
        tv2.setAnimation(rightAnim);
        final Intent intent = new Intent(AlarmActivity.this, AlarmReceiver.class);
        btnHenGio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDung.setEnabled(true);
                calendar.set(Calendar.HOUR_OF_DAY, timePicker.getHour());
                calendar.set(Calendar.MINUTE, timePicker.getMinute());

                int gio = timePicker.getHour();
                int phut = timePicker.getMinute();
                String strGio = String.valueOf(gio);
                String strPhut = String.valueOf(phut);
                intent.putExtra("extra", "on");
                pendingIntent = PendingIntent.getBroadcast(
                        AlarmActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
                );
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                Toast.makeText(getApplicationContext(), "Bạn đã đặt báo thức lúc " + strGio + ":" + strPhut, Toast.LENGTH_LONG).show();

            }
        });

        btnDung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim();
                alarmManager.cancel(pendingIntent);
                Screen_Question.dataChart.clear();
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intentS = new Intent(AlarmActivity.this, Screen_Question.class);
                            startActivity(intentS);
                        }
                    }, SPLASH_SCREEN);
                } catch (Exception ex) {
                    Log.d("wq", "onClick: "+ ex);
                }
            }
        });

        btnXemKQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Screen_Question.dataChart.size() > 0) {
                    anim();
                    try {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intentSS = new Intent(AlarmActivity.this, Screen_Result.class);
                                startActivity(intentSS);
                            }
                        }, SPLASH_SCREEN);
                    }catch (Exception ex) {
                        Log.d("err", "onClick: " + ex);
                    }
                } else Toast.makeText(getApplicationContext(),"Bạn chưa trả lời câu hỏi", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setControl() {
        btnDung = findViewById(R.id.btnDung);
        btnHenGio = findViewById(R.id.btnHenGio);
        btnXemKQ = findViewById(R.id.btnXemKQ);
        timePicker = findViewById(R.id.timePicker);
        calendar = Calendar.getInstance();
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tvSlogan1 = findViewById(R.id.tvSlogan1);
        tvSlogan2 = findViewById(R.id.tvSlogan2);
        btnDung.setEnabled(false);

        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        outTopAnim = AnimationUtils.loadAnimation(this, R.anim.out_top_animation);
        outBottomAnim = AnimationUtils.loadAnimation(this, R.anim.out_bottom_animation);
        rightAnim = AnimationUtils.loadAnimation(this, R.anim.right_animation);
        leftAnim = AnimationUtils.loadAnimation(this, R.anim.left_animation);
        outRightAnim = AnimationUtils.loadAnimation(this, R.anim.out_right_animation);
        outLeftAnim = AnimationUtils.loadAnimation(this, R.anim.out_left_animation);
    }

    private void anim() {
        tvSlogan1.startAnimation(outTopAnim);
        tvSlogan2.startAnimation(outTopAnim);
        timePicker.startAnimation(outRightAnim);
        btnDung.startAnimation(outBottomAnim);
        btnHenGio.startAnimation(outBottomAnim);
        btnXemKQ.startAnimation(outBottomAnim);
        tv1.startAnimation(outLeftAnim);
        tv2.startAnimation(outRightAnim);
    }
}
