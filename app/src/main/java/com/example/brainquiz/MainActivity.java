package com.example.brainquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN = 500;

    Animation topAnim, bottomAnim, outTopAnim, outBottomAnim;
    ImageView imgIconStart;
    TextView tvLogo, tvSolgan;
    Button btnStart, btnResult;

    DapAnDB dapAnDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        setControl();
        setEvent();
    }

    private void setEvent() {
        imgIconStart.setAnimation(topAnim);
        tvLogo.setAnimation(bottomAnim);
        tvSolgan.setAnimation(bottomAnim);
        btnStart.setAnimation(bottomAnim);
        btnResult.setAnimation(bottomAnim);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim();
                dapAnDB = new DapAnDB(getApplicationContext());
                dapAnDB.xoaTatCaDapAn();
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(MainActivity.this, Screen_Question.class);
                            startActivity(intent);
                        }
                    }, SPLASH_SCREEN);
                } catch (Exception ex) {
                    Log.d("wq", "onClick: "+ ex);
                }


            }
        });

        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim();

                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(MainActivity.this, Screen_Result.class);
                            startActivity(intent);
                        }
                    }, SPLASH_SCREEN);
                }catch (Exception ex) {
                    Log.d("err", "onClick: " + ex);
                }
            }
        });
    }

    private void setControl() {
        imgIconStart = findViewById(R.id.imgIconStart);
        tvLogo = findViewById(R.id.tvLogo);
        tvSolgan = findViewById(R.id.tvSlogan);
        btnStart = findViewById(R.id.btnStart);
        btnResult = findViewById(R.id.btnResult);

        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        outTopAnim = AnimationUtils.loadAnimation(this, R.anim.out_top_animation);
        outBottomAnim = AnimationUtils.loadAnimation(this, R.anim.out_bottom_animation);
    }

    private void anim() {
        imgIconStart.startAnimation(outTopAnim);
        tvLogo.startAnimation(outBottomAnim);
        tvSolgan.startAnimation(outBottomAnim);
        btnStart.startAnimation(outBottomAnim);
        btnResult.startAnimation(outBottomAnim);
    }
}
