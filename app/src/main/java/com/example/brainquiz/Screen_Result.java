package com.example.brainquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Screen_Result extends AppCompatActivity {

    ListView lsvShow;
    TextView tvLabel;
    ArrayList<DapAn> listDA = new ArrayList<>();
    DapAnDB dapAnDB;
    Animation bottomAnim, topAnim;
    //chart
    BarChart barChart;
    ArrayList<BarEntry> barEntryArrayList;
    ArrayList<String> labelNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view_dap_an);
        setControl();
        setEvent();
    }

    private void setEvent() {
        tvLabel.setAnimation(topAnim);
        lsvShow.setAnimation(bottomAnim);
        dapAnDB = new DapAnDB(this);

        barEntryArrayList = new ArrayList<>();
        labelNames = new ArrayList<>();
        for(int i = 0; i < Screen_Question.dataChart.size(); i++) {
            String cauHoi = Screen_Question.dataChart.get(i).getCauHoi();
            int thoiGian = Screen_Question.dataChart.get(i).getThoiGian();
            barEntryArrayList.add(new BarEntry(i, thoiGian));
            labelNames.add(cauHoi);
        }
        BarDataSet barDataSet = new BarDataSet(barEntryArrayList, "Thời gian trả lời câu hỏi");
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        Description description = new Description();
        description.setText("Thời gian (giây)");
        barChart.setDescription(description);
        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labelNames));

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(labelNames.size());
        xAxis.setLabelRotationAngle(270);
        barChart.animateY(2000);
        barChart.invalidate();

        Cursor cursor = dapAnDB.layDapAn();
        if (cursor != null) {
            listDA.clear();
            while (cursor.moveToNext()) {
                DapAn dapAn = new DapAn();
                dapAn.setCauHoi(cursor.getString(cursor.getColumnIndex(DBHelper.COT_CAUHOIDA)));
                dapAn.setTraLoi1(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOIDA1)));
                dapAn.setTraLoi2(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOIDA2)));
                dapAn.setTraLoi3(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOIDA3)));
                dapAn.setTraLoi4(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOIDA4)));
                dapAn.setDapAn(cursor.getInt(cursor.getColumnIndex(DBHelper.COT_DAPANDA)));
                dapAn.setDapAnCuaBan(cursor.getInt(cursor.getColumnIndex(DBHelper.COT_DAPANDACB)));
                listDA.add(dapAn);
            }
        }

        final CustomAdapter adapterListDA = new CustomAdapter(this, R.layout.list_view_custom, listDA);
        lsvShow.setAdapter(adapterListDA);
    }

    private void setControl() {
        lsvShow = findViewById(R.id.lsvDapAn);
        tvLabel = findViewById(R.id.tvLabel);
        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        barChart = findViewById(R.id.barChart);
    }
}
