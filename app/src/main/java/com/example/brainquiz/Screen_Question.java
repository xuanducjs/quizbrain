package com.example.brainquiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class Screen_Question extends AppCompatActivity {
    public static final long COUNTDOWN = 15000;
    public static final long COUNTDOWNNEXT = 3000;

    Animation leftAnim, rightAnim, outLeftAnim, outRightAnim, topAnim, bottomAnim, zoomIn, fadeIn;
    TextView tvCauHoi, tvDemNguoc, tvNotify, tvDiem, tvCauHoiSo;
    RadioGroup rdGroup;
    AppCompatRadioButton rdbTraLoi1, rdbTraLoi2, rdbTraLoi3, rdbTraLoi4;
    Button btnConfirm;
    ImageView imgNotify, imgCauHoi;

    ColorStateList textColorDefaultRb, textColorDefaultCd;

    CountDownTimer countDownTimer;
    long timeLeft, timeLeftNext;

    ArrayList<CauHoi> listCauHoi = new ArrayList<>();
    public static ArrayList<QuestionData> dataChart = new ArrayList<>();
    CauHoiDB cauHoiDB;
    CauHoi cauHoiHienTai;

    DapAnDB dapAnDB;

    int demCauHoi = 0;
    int soCauDung = 0;
    int tongSoCauHoi;
    int diem = 0;
    boolean traLoi;
    boolean thayDoiCauHoi = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen__question);
        setControl();
        setEvent();
    }

    private void setEvent() {
        cauHoiDB = new CauHoiDB(this);
        dapAnDB = new DapAnDB(this);
//        cauHoiDB.nhapCauHoi();
        dapAnDB.xoaTatCaDapAn();
        rdbTraLoi1.setAnimation(rightAnim);
        rdbTraLoi2.setAnimation(leftAnim);
        rdbTraLoi3.setAnimation(rightAnim);
        rdbTraLoi4.setAnimation(leftAnim);
        btnConfirm.setAnimation(bottomAnim);
        tvDemNguoc.setAnimation(topAnim);
        tvCauHoi.setAnimation(topAnim);
        tvDiem.setAnimation(topAnim);
        tvCauHoiSo.setAnimation(topAnim);
        imgCauHoi.setAnimation(topAnim);
        updateData();
        textColorDefaultRb = rdbTraLoi1.getTextColors();
        textColorDefaultCd = tvDemNguoc.getTextColors();
        tongSoCauHoi = listCauHoi.size();
        Collections.shuffle(listCauHoi);
        hienThiCauHoiTiepTheo();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSelectedRadioButton();
                if (!traLoi) {
                    if (rdbTraLoi1.isChecked() || rdbTraLoi2.isChecked() || rdbTraLoi3.isChecked() || rdbTraLoi4.isChecked()) {
                        kiemTraDapAn();
                    }
                    else {
                        imgNotify.setImageResource(R.drawable.choose);
                        tvNotify.setText("Chọn đáp án bạn ơi");
                        imgNotify.startAnimation(zoomIn);
                        tvNotify.startAnimation(zoomIn);
                    }
                }
                else {
                    countDownTimer.cancel();
                    hienThiCauHoiTiepTheo();
                }
            }
        });
    }

    private void kiemTraDapAn() {
        traLoi = true;
        countDownTimer.cancel();
        RadioButton rdbChon = findViewById(rdGroup.getCheckedRadioButtonId());
        int dapAn = rdGroup.indexOfChild(rdbChon) + 1;
        DapAn dapAn1 = new DapAn();

        Log.d("daa", "kiemTraDapAn: " + timeLeft);
        int round = Math.round((COUNTDOWN - timeLeft) / 1000);
        QuestionData qs = new QuestionData();
        qs.setCauHoi("Câu " + demCauHoi);
        qs.setThoiGian(round);
        dataChart.add(qs);

        if (dapAn == 0) {
            switch (cauHoiHienTai.getDapAn()) {
                case 1:
                    rdbTraLoi1.setBackground(getDrawable(R.drawable.chua_tra_loi));
                    rdbTraLoi1.setTextColor(getColor(R.color.colorWhite));
                    break;
                case 2:
                    rdbTraLoi2.setBackground(getDrawable(R.drawable.chua_tra_loi));
                    rdbTraLoi2.setTextColor(getColor(R.color.colorWhite));
                    break;
                case 3:
                    rdbTraLoi3.setBackground(getDrawable(R.drawable.chua_tra_loi));
                    rdbTraLoi3.setTextColor(getColor(R.color.colorWhite));
                    break;
                case 4:
                    rdbTraLoi4.setBackground(getDrawable(R.drawable.chua_tra_loi));
                    rdbTraLoi4.setTextColor(getColor(R.color.colorWhite));
                    break;
            }
            imgNotify.setImageResource(R.drawable.wrong);
            tvNotify.setText("Bạn đâu rồi nhỉ?");
            tvNotify.startAnimation(zoomIn);
            imgNotify.startAnimation(zoomIn);
        }

        if (dapAn == cauHoiHienTai.getDapAn()) {
            soCauDung++;
            if (timeLeft > 10000) {
                diem+=3;
            }
            if (timeLeft > 5000 && timeLeft <= 10000) {
                diem+=2;
            }
            if (timeLeft <= 5000) {
                diem+=1;
            }
            tvDiem.setText("Điểm: " + diem);

            rdbChon.setBackground(getDrawable(R.drawable.tra_loi_dung));
            rdbChon.setTextColor(getColor(R.color.colorWhite));
            imgNotify.setImageResource(R.drawable.correct);
            tvNotify.setText("Tính ra mình cũng giỏi đó chứ");
            imgNotify.startAnimation(zoomIn);
            tvNotify.startAnimation(zoomIn);
        }
        if (dapAn != 0 && dapAn != cauHoiHienTai.getDapAn()) {
            rdbChon.setBackground(getDrawable(R.drawable.tra_loi_sai));
            rdbChon.setTextColor(getColor(R.color.colorWhite));
            imgNotify.setImageResource(R.drawable.wrong);
            tvNotify.setText("Hừm, sai câu này thì ta làm câu khác");
            tvNotify.startAnimation(zoomIn);
            imgNotify.startAnimation(zoomIn);
            switch (cauHoiHienTai.getDapAn()) {
                case 1:
                    rdbTraLoi1.setBackground(getDrawable(R.drawable.tra_loi_dung));
                    rdbTraLoi1.setTextColor(getColor(R.color.colorWhite));
                    break;
                case 2:
                    rdbTraLoi2.setBackground(getDrawable(R.drawable.tra_loi_dung));
                    rdbTraLoi2.setTextColor(getColor(R.color.colorWhite));
                    break;
                case 3:
                    rdbTraLoi3.setBackground(getDrawable(R.drawable.tra_loi_dung));
                    rdbTraLoi3.setTextColor(getColor(R.color.colorWhite));
                    break;
                case 4:
                    rdbTraLoi4.setBackground(getDrawable(R.drawable.tra_loi_dung));
                    rdbTraLoi4.setTextColor(getColor(R.color.colorWhite));
                    break;
            }
        }
        dapAn1.setCauHoi(cauHoiHienTai.getCauHoi());
        dapAn1.setTraLoi1(cauHoiHienTai.getTraLoi1());
        dapAn1.setTraLoi2(cauHoiHienTai.getTraLoi2());
        dapAn1.setTraLoi3(cauHoiHienTai.getTraLoi3());
        dapAn1.setTraLoi4(cauHoiHienTai.getTraLoi4());
        dapAn1.setDapAn(cauHoiHienTai.getDapAn());
        dapAn1.setDapAnCuaBan(dapAn);
        dapAnDB.themDapAn(dapAn1);
        xacNhanDapAn();
    }

    private void xacNhanDapAn() {
        if (demCauHoi < tongSoCauHoi) {
            btnConfirm.setText("Câu tiếp theo đi");
            thayDoiCauHoi = true;
            timeLeftNext = COUNTDOWNNEXT;
            batDauDemNguocNext();
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Kết quả");
            builder.setMessage("Số câu đúng: " + soCauDung + "/" + tongSoCauHoi + "\n" + "Tổng số điểm đạt được: " + diem);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finishQuiz();
                    if (soCauDung >= 7) {
                        Intent myIntent = new Intent(Screen_Question.this, AlarmReceiver.class);
                        myIntent.putExtra("extra", "off");
                        sendBroadcast(myIntent);
                    }
                }
            });
            builder.show();
        }
    }

    private void resetSelectedRadioButton() {
        rdbTraLoi1.setBackground(getDrawable(R.drawable.rdb_tra_loi_1_selector));
        rdbTraLoi2.setBackground(getDrawable(R.drawable.rdb_tra_loi_1_selector));
        rdbTraLoi3.setBackground(getDrawable(R.drawable.rdb_tra_loi_1_selector));
        rdbTraLoi4.setBackground(getDrawable(R.drawable.rdb_tra_loi_1_selector));
        tvNotify.setText("");
        imgNotify.setImageResource(0);
        if (thayDoiCauHoi) {
            tvCauHoi.startAnimation(fadeIn);
            rdbTraLoi1.startAnimation(rightAnim);
            rdbTraLoi2.startAnimation(leftAnim);
            rdbTraLoi3.startAnimation(rightAnim);
            rdbTraLoi4.startAnimation(leftAnim);
        }
    }

    private void setControl() {
        tvCauHoi = findViewById(R.id.tvCauHoi);
        tvDemNguoc = findViewById(R.id.tvDemNguoc);
        tvNotify = findViewById(R.id.tvNotify);
        tvCauHoiSo = findViewById(R.id.tvCauHoiSo);
        tvDiem = findViewById(R.id.tvDiem);
        rdGroup = findViewById(R.id.rdGroup);
        rdbTraLoi1 = findViewById(R.id.rdbTraLoi1);
        rdbTraLoi2 = findViewById(R.id.rdbTraLoi2);
        rdbTraLoi3 = findViewById(R.id.rdbTraLoi3);
        rdbTraLoi4 = findViewById(R.id.rdbTraLoi4);
        btnConfirm = findViewById(R.id.btnConfirm);
        imgNotify = findViewById(R.id.imgNotify);
        imgCauHoi = findViewById(R.id.imgCauHoi);

        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        rightAnim = AnimationUtils.loadAnimation(this, R.anim.right_animation);
        leftAnim = AnimationUtils.loadAnimation(this, R.anim.left_animation);
        outRightAnim = AnimationUtils.loadAnimation(this, R.anim.out_right_animation);
        outLeftAnim = AnimationUtils.loadAnimation(this, R.anim.out_left_animation);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in_animation);
        zoomIn = AnimationUtils.loadAnimation(this, R.anim.zoom_in_animation);
    }

    public void onRadioButtonClicked(View view) {
        boolean isSelected = ((AppCompatRadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.rdbTraLoi1:
                if (isSelected) {
                    rdbTraLoi1.setTextColor(getColor(R.color.colorWhite));
                    rdbTraLoi2.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi3.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi4.setTextColor(getColor(R.color.colorChecked));
                }
                break;
            case R.id.rdbTraLoi2:
                if (isSelected) {
                    rdbTraLoi1.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi2.setTextColor(getColor(R.color.colorWhite));
                    rdbTraLoi3.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi4.setTextColor(getColor(R.color.colorChecked));
                }
                break;
            case R.id.rdbTraLoi3:
                if (isSelected) {
                    rdbTraLoi1.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi2.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi3.setTextColor(getColor(R.color.colorWhite));
                    rdbTraLoi4.setTextColor(getColor(R.color.colorChecked));
                }
                break;
            case R.id.rdbTraLoi4:
                if (isSelected) {
                    rdbTraLoi1.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi2.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi3.setTextColor(getColor(R.color.colorChecked));
                    rdbTraLoi4.setTextColor(getColor(R.color.colorWhite));
                }
                break;
        }
    }

    private void hienThiCauHoiTiepTheo() {
        rdbTraLoi1.setTextColor(textColorDefaultRb);
        rdbTraLoi2.setTextColor(textColorDefaultRb);
        rdbTraLoi3.setTextColor(textColorDefaultRb);
        rdbTraLoi4.setTextColor(textColorDefaultRb);
        rdGroup.clearCheck();

        if (demCauHoi < tongSoCauHoi) {
            cauHoiHienTai = listCauHoi.get(demCauHoi);
            tvCauHoi.setText(cauHoiHienTai.getCauHoi());
            rdbTraLoi1.setText(cauHoiHienTai.getTraLoi1());
            rdbTraLoi2.setText(cauHoiHienTai.getTraLoi2());
            rdbTraLoi3.setText(cauHoiHienTai.getTraLoi3());
            rdbTraLoi4.setText(cauHoiHienTai.getTraLoi4());
            demCauHoi++;
            traLoi = false;
            tvCauHoiSo.setText("Câu hỏi: " + demCauHoi + "/" + tongSoCauHoi);
            btnConfirm.setText("Chốt đáp án nè");
            thayDoiCauHoi = false;

            timeLeft = COUNTDOWN;
            batDauDemNguoc();
        } else {
            finishQuiz();
        }
    }

    private void batDauDemNguoc() {
        countDownTimer = new CountDownTimer(timeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeft = millisUntilFinished;
                capNhatDemNguoc();
            }

            @Override
            public void onFinish() {
                timeLeft = 0;
                capNhatDemNguoc();
                kiemTraDapAn();
            }
        }.start();
    }

    private void batDauDemNguocNext() {
        countDownTimer = new CountDownTimer(timeLeftNext, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftNext = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                timeLeftNext = 0;
                resetSelectedRadioButton();
                hienThiCauHoiTiepTheo();
            }
        }.start();
    }

    private void capNhatDemNguoc() {
        int phut = (int) (timeLeft / 1000) / 60;
        int giay = (int) (timeLeft / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", phut, giay);
        tvDemNguoc.setText(timeFormatted);

        if (timeLeft < 6000) {
            tvDemNguoc.setTextColor(Color.RED);
        } else {
            tvDemNguoc.setTextColor(textColorDefaultCd);
        }
    }

    private void finishQuiz() {
        finish();
    }

    private void updateData() {
        Cursor cursor = cauHoiDB.layCauHoi();
        if (cursor != null) {
            listCauHoi.clear();
            while (cursor.moveToNext()) {
                CauHoi cauHoi = new CauHoi();
                cauHoi.setCauHoi(cursor.getString(cursor.getColumnIndex(DBHelper.COT_CAUHOI)));
                cauHoi.setTraLoi1(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOI1)));
                cauHoi.setTraLoi2(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOI2)));
                cauHoi.setTraLoi3(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOI3)));
                cauHoi.setTraLoi4(cursor.getString(cursor.getColumnIndex(DBHelper.COT_TRALOI4)));
                cauHoi.setDapAn(cursor.getInt(cursor.getColumnIndex(DBHelper.COT_DAPAN)));
                listCauHoi.add(cauHoi);
            }
        }
    }

    private void anim() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
