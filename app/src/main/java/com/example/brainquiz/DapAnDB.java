package com.example.brainquiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DapAnDB {
    SQLiteDatabase database;
    DBHelper dbHelper;

    public DapAnDB(Context context) {
        dbHelper = new DBHelper(context);
        try {
            database = dbHelper.getWritableDatabase();
        }
        catch (SQLiteException ex) {
            database = dbHelper.getReadableDatabase();
        }
    }

    public long themDapAn(DapAn dapAn) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.COT_CAUHOIDA, dapAn.getCauHoi());
        values.put(DBHelper.COT_TRALOIDA1, dapAn.getTraLoi1());
        values.put(DBHelper.COT_TRALOIDA2, dapAn.getTraLoi2());
        values.put(DBHelper.COT_TRALOIDA3, dapAn.getTraLoi3());
        values.put(DBHelper.COT_TRALOIDA4, dapAn.getTraLoi4());
        values.put(DBHelper.COT_DAPANDA, dapAn.getDapAn());
        values.put(DBHelper.COT_DAPANDACB, dapAn.getDapAnCuaBan());
        return database.insert(DBHelper.TEN_BANG_DAPAN, null, values);
    }

    public Cursor layDapAn() {
        // Biến cot là khai báo danh sách các cột cần lấy.
        String[] cot = {
                DBHelper.COT_MACAUHOIDA,
                DBHelper.COT_CAUHOIDA,
                DBHelper.COT_TRALOIDA1,
                DBHelper.COT_TRALOIDA2,
                DBHelper.COT_TRALOIDA3,
                DBHelper.COT_TRALOIDA4,
                DBHelper.COT_DAPANDA,
                DBHelper.COT_DAPANDACB
        };
        Cursor cursor = null;
        cursor = database.query(DBHelper.
                TEN_BANG_DAPAN, cot, null, null, null, null, DBHelper.COT_MACAUHOIDA + " ASC");
        return cursor;
    }

    public void xoaTatCaDapAn() {
        database.delete(DBHelper.TEN_BANG_DAPAN, null, null);
    }
}
