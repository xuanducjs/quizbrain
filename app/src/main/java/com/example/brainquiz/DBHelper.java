package com.example.brainquiz;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    // Tên cơ sở dữ liệu
    public static final String TEN_DATABASE = "BrainQuiz";
    // Tên bảng
    public static final String TEN_BANG_CAUHOI = "CauHoi";

    public static final String COT_MACAUHOI = "_maCauHoi";
    public static final String COT_CAUHOI = "_cauHoi";
    public static final String COT_TRALOI1 = "_traLoi1";
    public static final String COT_TRALOI2 = "_traLoi2";
    public static final String COT_TRALOI3 = "_traLoi3";
    public static final String COT_TRALOI4 = "_traLoi4";
    public static final String COT_DAPAN = "_dapAn";

    private static final String TAO_BANG_CAUHOI = ""
            + "create table " + TEN_BANG_CAUHOI + " ( "
            + COT_MACAUHOI + " integer primary key autoincrement ,"
            + COT_CAUHOI + " text not null, "
            + COT_TRALOI1 + " text not null, "
            + COT_TRALOI2 + " text not null, "
            + COT_TRALOI3 + " text not null, "
            + COT_TRALOI4 + " text not null, "
            + COT_DAPAN + " integer not null );";

    // Tên bảng
    public static final String TEN_BANG_DAPAN = "DapAn";

    public static final String COT_MACAUHOIDA = "_maCauHoiDA";
    public static final String COT_CAUHOIDA = "_cauHoiDA";
    public static final String COT_TRALOIDA1 = "_traLoiDA1";
    public static final String COT_TRALOIDA2 = "_traLoiDA2";
    public static final String COT_TRALOIDA3 = "_traLoiDA3";
    public static final String COT_TRALOIDA4 = "_traLoiDA4";
    public static final String COT_DAPANDA = "_dapAnDA";
    public static final String COT_DAPANDACB = "_dapAnCuaBan";

    private static final String TAO_BANG_DAPAN = ""
            + "create table " + TEN_BANG_DAPAN + " ( "
            + COT_MACAUHOIDA + " integer primary key autoincrement ,"
            + COT_CAUHOIDA + " text not null, "
            + COT_TRALOIDA1 + " text not null, "
            + COT_TRALOIDA2 + " text not null, "
            + COT_TRALOIDA3 + " text not null, "
            + COT_TRALOIDA4 + " text not null, "
            + COT_DAPANDA + " integer not null, "
            + COT_DAPANDACB + " integer not null );";


    public DBHelper(@Nullable Context context) {
        super(context, TEN_DATABASE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TAO_BANG_CAUHOI);
        db.execSQL(TAO_BANG_DAPAN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
