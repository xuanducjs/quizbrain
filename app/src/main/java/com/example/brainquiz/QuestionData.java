package com.example.brainquiz;

public class QuestionData {
    String cauHoi;
    int thoiGian;

    public QuestionData() {
    }

    public QuestionData(String cauHoi, int thoiGian) {
        this.cauHoi = cauHoi;
        this.thoiGian = thoiGian;
    }

    public String getCauHoi() {
        return cauHoi;
    }

    public void setCauHoi(String cauHoi) {
        this.cauHoi = cauHoi;
    }

    public int getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(int thoiGian) {
        this.thoiGian = thoiGian;
    }
}
