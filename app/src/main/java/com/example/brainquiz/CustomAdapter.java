package com.example.brainquiz;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.brainquiz.DapAn;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter {

    Context context;
    int layoutId;
    ArrayList<DapAn> listDA = new ArrayList<>();

    public CustomAdapter(@NonNull Context context, int layoutId, @NonNull ArrayList<DapAn> listDA) {
        super(context, layoutId);
        this.context = context;
        this.layoutId = layoutId;
        this.listDA = listDA;
    }

    @Override
    public int getCount() {
        return listDA.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(layoutId, null);
        TextView tvCauHoi1 = convertView.findViewById(R.id.tvCauHoi1);
        TextView tvDapAn1 = convertView.findViewById(R.id.tvDapAn1);
        TextView tvDapAn2 = convertView.findViewById(R.id.tvDapAn2);
        TextView tvDapAn3 = convertView.findViewById(R.id.tvDapAn3);
        TextView tvDapAn4 = convertView.findViewById(R.id.tvDapAn4);
        ImageView imgResult = convertView.findViewById(R.id.imgResult);

        tvCauHoi1.setText("Câu hỏi: " + listDA.get(position).getCauHoi());
        tvDapAn1.setText("A. " + listDA.get(position).getTraLoi1());
        tvDapAn2.setText("B. " + listDA.get(position).getTraLoi2());
        tvDapAn3.setText("C. " + listDA.get(position).getTraLoi3());
        tvDapAn4.setText("D. " + listDA.get(position).getTraLoi3());

        switch (listDA.get(position).getDapAnCuaBan()) {
            case 0:
                imgResult.setImageResource(R.drawable.wrong);
                switch (listDA.get(position).getDapAn()) {
                    case 1:
                        tvDapAn1.setTextColor(Color.WHITE);
                        tvDapAn1.setBackgroundColor(Color.MAGENTA);
                        break;
                    case 2:
                        tvDapAn2.setTextColor(Color.WHITE);
                        tvDapAn2.setBackgroundColor(Color.MAGENTA);
                        break;
                    case 3:
                        tvDapAn3.setTextColor(Color.WHITE);
                        tvDapAn3.setBackgroundColor(Color.MAGENTA);
                        break;
                    case 4:
                        tvDapAn4.setTextColor(Color.WHITE);
                        tvDapAn4.setBackgroundColor(Color.MAGENTA);
                        break;
                }
                break;
            case 1:
                switch (listDA.get(position).getDapAn()) {
                    case 1:
                        tvDapAn1.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.correct);
                        tvDapAn1.setBackgroundColor(Color.GREEN);
                        break;
                    case 2:
                        tvDapAn1.setTextColor(Color.WHITE);
                        tvDapAn2.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn1.setBackgroundColor(Color.RED);
                        tvDapAn2.setBackgroundColor(Color.GREEN);
                        break;
                    case 3:
                        tvDapAn1.setTextColor(Color.WHITE);
                        tvDapAn3.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn1.setBackgroundColor(Color.RED);
                        tvDapAn3.setBackgroundColor(Color.GREEN);
                        break;
                    case 4:
                        tvDapAn1.setTextColor(Color.WHITE);
                        tvDapAn4.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn1.setBackgroundColor(Color.RED);
                        tvDapAn4.setBackgroundColor(Color.GREEN);
                        break;
                }
                break;
            case 2:
                switch (listDA.get(position).getDapAn()) {
                    case 1:
                        tvDapAn2.setTextColor(Color.WHITE);
                        tvDapAn1.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn2.setBackgroundColor(Color.RED);
                        tvDapAn1.setBackgroundColor(Color.GREEN);
                        break;
                    case 2:
                        tvDapAn1.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.correct);
                        tvDapAn2.setBackgroundColor(Color.GREEN);
                        break;
                    case 3:
                        tvDapAn2.setTextColor(Color.WHITE);
                        tvDapAn3.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn2.setBackgroundColor(Color.RED);
                        tvDapAn3.setBackgroundColor(Color.GREEN);
                        break;
                    case 4:
                        tvDapAn2.setTextColor(Color.WHITE);
                        tvDapAn4.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn2.setBackgroundColor(Color.RED);
                        tvDapAn4.setBackgroundColor(Color.GREEN);
                        break;
                }
                break;
            case 3:
                switch (listDA.get(position).getDapAn()) {
                    case 1:
                        tvDapAn1.setTextColor(Color.WHITE);
                        tvDapAn3.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn1.setBackgroundColor(Color.GREEN);
                        tvDapAn3.setBackgroundColor(Color.RED);
                        break;
                    case 2:
                        tvDapAn2.setTextColor(Color.WHITE);
                        tvDapAn3.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn2.setBackgroundColor(Color.GREEN);
                        tvDapAn3.setBackgroundColor(Color.RED);
                        break;
                    case 3:
                        tvDapAn3.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.correct);
                        tvDapAn3.setBackgroundColor(Color.GREEN);
                        break;
                    case 4:
                        tvDapAn4.setTextColor(Color.WHITE);
                        tvDapAn3.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn4.setBackgroundColor(Color.GREEN);
                        tvDapAn3.setBackgroundColor(Color.RED);
                        break;
                }
                break;
            case 4:
                switch (listDA.get(position).getDapAn()) {
                    case 1:
                        tvDapAn1.setTextColor(Color.WHITE);
                        tvDapAn4.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn1.setBackgroundColor(Color.GREEN);
                        tvDapAn4.setBackgroundColor(Color.RED);
                        break;
                    case 2:
                        tvDapAn2.setTextColor(Color.WHITE);
                        tvDapAn4.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn2.setBackgroundColor(Color.GREEN);
                        tvDapAn4.setBackgroundColor(Color.RED);
                        break;
                    case 3:
                        tvDapAn3.setTextColor(Color.WHITE);
                        tvDapAn4.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.wrong);
                        tvDapAn3.setBackgroundColor(Color.GREEN);
                        tvDapAn4.setBackgroundColor(Color.RED);
                        break;
                    case 4:
                        tvDapAn4.setTextColor(Color.WHITE);
                        imgResult.setImageResource(R.drawable.correct);
                        tvDapAn4.setBackgroundColor(Color.GREEN);
                        break;
                }
                break;
        }

        return convertView;
    }
}
